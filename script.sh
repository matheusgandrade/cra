npx create-react-app $1 
cd $1
yarn add react-router-dom 
commitizen init cz-conventional-changelog --yarn --dev --exact 
yarn add react-hook-form
yarn add yup @hookform/resolvers
yarn add @material-ui/core
yarn add @material-ui/icons
yarn add react-redux redux
yarn add styled-components
yarn add redux-thunk
rm README.md
cd src
rm setupTests.js
rm reportWebVitals.js
rm logo.svg
rm App.test.js
mkdir store
cd store
touch index.js
echo "import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from './modules/module1/reducers';

const reducers = combineReducers({ reducer });

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
">index.js
mkdir modules
cd modules
mkdir module1
cd module1
touch types.js
echo "export const Type = 'type'"
touch actions.js
echo "//import { Type } from './types';

export const action = (list) => {
  return {
    type: type,
    list:list ,
  };
};"> actions.js

touch reducers.js
echo "import { Type } from './types';

const defaultState = { state: [] };

const reducer = (state = defaultState, action) => {
  const { list, type } = action;

  switch (type) {
    case Type:
      return { state: list };

    default:
      return state;
  }
};

export default reducer;
">reducers.js
touch thunks.js
echo "import { action } from './action';

export const actionThunk = () => (dispatch, getState) => {
  const list = [];
  dispatch(action(list));
};">thunks.js
cd ..
cd ..
cd ..
echo "import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);">index.js
echo "import { useState, useEffect } from 'react';
import Routers from './Routers';

const App = () => {

  return (
    <div className='App'>
      <Routers />
    </div>
  );
};

export default App;"> App.js
mkdir Routers
cd Routers
touch index.js
echo "import { Route, Switch, useHistory } from 'react-router-dom';

const Routers = () => {
  
  return (
    <>
      <Switch>
        <Route exact path='/'>
          <h1>Bora Codar</h1>
        </Route>
      </Switch>
    </>
  );
};
export default Routers;
">index.js

yarn start

